<?php

namespace Vitrin\SDK\Auth\Services;

use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class PermissionService
{
    protected bool $is_loaded = false;

    protected string $project = '';

    protected string $prefix = '';


    protected $permissions = [];

    public function setProject(string $name)
    {
        $this->project = $name ? "{$name}." : '';
    }

    public function setPrefix(string $name)
    {
        $this->prefix = $name ? "{$name}." : '';
    }

    public function setPermissions(array $permissions)
    {
        $this->is_loaded = true;

        $this->permissions = collect($permissions);
    }

    public function addPermission(string $permission)
    {
        !App::runningUnitTests() && throw new Exception('addPermission method only available on tests !');

        $this->permissions->add("{$this->project}{$this->prefix}{$permission}");
    }

    private function isWildcardMatch($pattern, $string) {
        // Escape special characters in the pattern except the wildcard character (*)
        $pattern = preg_quote($pattern, '/');
    
        // Replace the wildcard character (*) with a regex pattern to match any character except a dot (.)
        $pattern = str_replace('\*', '[^.]*', $pattern);

        // Use the ^ and $ to match the whole string, not just any part of it
        $pattern = "/^" . $pattern . "$/";
    
        // Use preg_match to check if the pattern matches the string
        return preg_match($pattern, $string) === 1;
    }

    public function beAbleTo(string $cq, string $entity)
    {
        !Auth::check() && App::abort(401);
        
        $res = $this->can("{$entity}.{$cq}");

        !$res && App::abort(403, "You don't have permission to " . "{$this->project}{$this->prefix}{$entity}.{$cq}");

        return $res;
    }

    public function can(string $permission):  bool
    {
        !$this->is_loaded && throw new Exception('permissions not loaded !');

        return !!$this->permissions->firstWhere(fn($p) => $this->isWildcardMatch($p, "{$this->project}{$this->prefix}{$permission}"));
    }
}