<?php

namespace Vitrin\SDK\Auth\Providers;

use Exception;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;
use Vitrin\SDK\Auth\Entities\User;
use Vitrin\SDK\Auth\Facades\Permission;

class VitrinUserProvider implements UserProvider
{
    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed  $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        throw new Exception('retrieve user by id not supported on vitrin provider');
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed  $identifier
     * @param  string  $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        if (App::isLocal())
        {
            $name = shell_exec('git config --global user.name');
            $first_name = trim(explode(' ', $name)[0]);
            $last_name = trim(str_replace($first_name, '', $name));
            
            Permission::setPermissions([
                '*',
                '*.*',
                '*.*.*',
                '*.*.*.*',
            ]);

            return User::from([
                'id'    => 1,
                'first_name'    => $first_name,
                'last_name'     => $last_name,
                'display_name'  => $name,
                'mobile'        => '+989123456789',
                'email'         => shell_exec('git config --global user.email'),
                'company'       => 'Vitrin Group',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ]);
        }
        
        if(App::runningUnitTests()) return null;

        !config('auth.providers.vitrin.base_url') && throw new Exception("Base url of auth server doesn't defined, check auth config file !");

        try {
            $res = Http::withHeader('Authorization', "Bearer $token")->acceptJson()->get(config('auth.providers.vitrin.base_url') . '/api/user');
        
            if ($res->status() !== 200) return null;

            Permission::setPermissions($res->json()['permissions']);
            
            return User::from($res->json()['user']);
        } catch (\Throwable $th) {
            return null;
        }
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  string  $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        throw new Exception('updateRememberToken is not supported on vitrin user provider');
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        throw new Exception('retrieve user by credentials not supported on vitrin provider');
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  array  $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        throw new Exception('validateCredentials is not supported on vitrin user provider');
    }
}