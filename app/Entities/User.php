<?php

namespace Vitrin\SDK\Auth\Entities;

use Illuminate\Contracts\Auth\Authenticatable;
use Spatie\LaravelData\Attributes\WithCastAndTransformer;
use Vitrin\Infrastructure\Core\Entity\CoreEntity;
use Vitrin\Infrastructure\Core\Entity\CoreEntityTrait;
use Vitrin\Infrastructure\Types\Id;
use Vitrin\Infrastructure\Types\IdMapper;

class User extends CoreEntity implements Authenticatable
{
    use CoreEntityTrait;

    public function __construct(
        #[WithCastAndTransformer(IdMapper::class)]
        public Id $id,
        public ?string $title,
        public ?string $first_name,
        public ?string $last_name,
        public ?string $display_name,
        public ?string $mobile,
        public ?string $email,
        public ?string $company,
        public ?string $job_title,
        public ?string $job_position,
        public ?string $national_code,
        public ?string $birthdate,
        public ?string $country,
        public ?string $address,
        public ?string $gender,
        public ?string $prefered_language,
        public ?string $prefered_theme,
        public ?string $password,
        public ?string $created_at,
        public ?string $updated_at,
    )
    {

    }

    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return $this->display_name;
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->id;
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return '';
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {
        //
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return '';
    }
}