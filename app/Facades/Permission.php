<?php

namespace Vitrin\SDK\Auth\Facades;

use Illuminate\Support\Facades\Facade;
use Vitrin\SDK\Auth\Services\PermissionService;

/**
 * @method static bool beAbleTo(string $cq, string $entity)
 * @method static bool can(string $permission)
 * @method static void setPermissions(array $permissions)
 * @method static void setProject(string $name)
 * @method static void setPrefix(string $name)
 */
class Permission extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     *
     * @throws \RuntimeException
     */
    protected static function getFacadeAccessor()
    {
        return PermissionService::class;
    }
}