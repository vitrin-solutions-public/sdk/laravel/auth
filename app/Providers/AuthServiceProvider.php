<?php

namespace Vitrin\SDK\Auth\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Vitrin\SDK\Auth\Guards\VitrinUserGuard;
use Vitrin\SDK\Auth\Services\PermissionService;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->app->singleton(PermissionService::class, function ($app) {
            return new PermissionService();
        });

        Auth::provider('vitrin', fn() => new VitrinUserProvider);

        Auth::extend('vitrin', function ($app, $name, array $config) {
            $provider = $app['auth']->createUserProvider($config['provider']);
        
            return new VitrinUserGuard($provider);
        });
    }
}
